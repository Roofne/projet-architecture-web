drop database if exists projet_sys;
create database projet_sys;
use projet_sys;

create table article (
    id_a integer not null auto_increment,
    marque varchar(255),
    prix float,
    category integer,
    photo varchar(255),
    primary key(id_a)
);

create table category (
    id_c integer not null auto_increment,
    name varchar(255),
    primary key(id_c)
);

create table panier (
    id_p integer not null auto_increment,
    id_c integer,
    total float,
    primary key(id_p)
);

create table user (
    id_u integer not null auto_increment,
    firstname varchar(255),
    surname varchar(255),
    username varchar(255),
    pwd varchar(255),
    isAdmin boolean,
    primary key(id_u)
);

/* valeurs par defaut */
insert into Category values(1,"ordinateurs");
insert into Category values(2,"telephonies");
insert into Category values(3,"stockage");

insert into Article values(null,"smartphone apple",300,1,"temp.png");
insert into Article values(null,"smartphone samsung",200,1,"temp.png");
insert into Article values(null,"ordi asus",400,2,"temp.png");
insert into Article values(null,"ordi apple",600,2,"temp.png");
insert into Article values(null,"ordi lenovo",500,2,"temp.png");
insert into Article values(null,"ssd kingston",100,3,"temp.png");
insert into Article values(null,"hdd kingston",25,3,"temp.png");

insert into User values(null,"admin","admin","admin","admin",true);
insert into User values(null,"root","root","root","root",true);


