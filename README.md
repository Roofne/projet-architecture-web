---
project_url: https://gitlab.com/Roofne/projet-architecture-web
branch: main
---

# Projet Architecture Web

![Build Status](https://img.shields.io/gitlab/pipeline-status/project-template4/project-template-java-maven?branch=main&style=plastic)
![Code Coverage](https://img.shields.io/gitlab/pipeline-coverage/project-template4/project-template-java-maven?branch=main)
![Test Result](https://img.shields.io/gitlab/pipeline-coverage/project-template4/project-template-java-maven?branch=main)
![Licence](https://img.shields.io/gitlab/license/project-template4/project-template-java-maven)
![Version](https://img.shields.io/gitlab/v/release/project-template4/project-template-java-maven?date_order_by=created_at&sort=date)
![Issue](https://img.shields.io/gitlab/issues/open-raw/project-template4/project-template-java-maven?gitlab_url=https%3A%2F%2Fgitlab.com)

## Description

Projet d'architecture web utilisant une API REST sous forme de servlet pour alimenter un site de commande en ligne.

## Prerequis

```txt
java 17
maven 3.8
tomcat apache 10
```

## Installation

1. Etape 1
   Cloner the repertoire
   ```sh
   git clone https://github.com/your_username_/Project-Name.git
   ```
2. Etape 2 : modifier le fichier .env-default en .env et ajouter vos informations personelles
3. Etape 3 : en cas de modification du code source, executer les commandes
```
mvn compiler:compile
mvn clean package
```
4. Etape 4 : Installer le serveur Apache et le démarrer
5. Etape 5 : Déployer l'archive projet.war sur le server
Soit dans l'IDE avec un gestionnaire de serveur
Soit sur l'adresse localhost:8080/ avec l'interface Tomcat

## Roadmap

- [x] Installation
- [x] Backend
- [ ] Frontend Angular
- [ ] Frontend Bootstrap

See the [open issues](https://gitlab.com/Roofne/projet-architecture-web/-/issues) for a full list of proposed features (and known issues).

## Contribution

## Auteurs

* **Toine** - [@Roofne](https://github.com/Roofne) 
* **Assem** - [@Assem](https://gitlab.com/Assem92) 
* **Lamia** - [@Lamia](https://gitlab.com/Lamia_ak) 

## References

Tutoriel YT : https://www.youtube.com/watch?v=xkKcdK1u95s&list=PLqq-6Pq4lTTZh5U8RbdXq0WaYvZBz2rbn&index=1

## License

## Status Projet

