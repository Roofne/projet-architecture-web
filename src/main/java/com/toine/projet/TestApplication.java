package com.toine.projet;

import com.toine.projet.controller.*;
import com.toine.projet.model.*;
import com.toine.projet.dao.*;
import com.toine.projet.resources.*;

public class TestApplication {
    public static void main(String[] args) {
        // Database onnection Controller
        BDDController bdd = new BDDController();

        // Article CRUD
        bdd.create("INSERT INTO Article VALUES(0,'Fanta',2.0,1,'temp.png')");
        bdd.select("SELECT * FROM Article WHERE id_a=8");
        bdd.update("UPDATE Article SET marque='Coca-Cola' WHERE id_a=8");
        bdd.delete("DELETE FROM Article WHERE id_a=8");

        // Categorie CRUD
        //bdd.insert("INSERT INTO Article VALUES(8,'Fanta',2.0,1,'temp.png')");
        bdd.select("SELECT * FROM Categorie WHERE id_c=1");
        //bdd.update("UPDATE Article SET marque='Coca-Cola' WHERE id_a=8");
        //bdd.delete("DELETE FROM Article WHERE id_a=8");
        
    }    
}