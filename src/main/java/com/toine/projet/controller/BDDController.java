package com.toine.projet.controller;

import java.sql.*;

public class BDDController {

    // Attributes
    String driver;
    String host;
    String username;
    String password;
    Connection connection;

    // Constructeurs
    public BDDController() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.driver     = "mysql";
            this.host       = "localhost/projet_sys";
            this.username   = "root";
            this.password   = "TPLa9c5!claudie";
            this.connection = DriverManager.getConnection("jdbc:"+driver+"://"+host,username,password);
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("ERROR: UPDATE YOUR CREDENTIAL IN BDDController");
        }   
    }

    // Getters et setters
    public String getUser() { return this.username; }
    public String getPassword() { return this.password; }

    // Methodes
    public void checkStatusCommand(String msg, Boolean status) {
        if(status == true) {
            System.out.println(msg+": SUCCESS");
        } else {
            System.out.println(msg+": FAILURE");
        }
    }

    //****************************//
    // SQL Command
    //****************************//

    // Select command
    public ResultSet select(String request) {
        ResultSet result = null;
        try {
            if(this.connection != null) {
                Statement statement = this.connection.createStatement();
                result = statement.executeQuery(request);     
                checkStatusCommand("SELECT",true);
            }
        } catch (SQLException e) {
            checkStatusCommand("SELECT",false);
        }
        return result;
    }

    // Insert command
    public void create(String request) {
        try {
            if(this.connection != null) {
                Statement statement = this.connection.createStatement();                
                statement.execute(request);
                checkStatusCommand("INSERT",true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Update command
    public void update(String request) {
        try {
            if(this.connection != null) {
                Statement statement = this.connection.createStatement();
                Boolean result = statement.execute(request);
                checkStatusCommand("UPDATE",true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Delete command
    public void delete(String request) {
        try {
            if(this.connection != null) {
                Statement statement = this.connection.createStatement();
                Boolean result = statement.execute(request);
                checkStatusCommand("DELETE",true);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

