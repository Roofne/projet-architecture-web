package com.toine.projet.resources;

import java.util.List;

import com.toine.projet.dao.CategoryDAO;
import com.toine.projet.model.Category;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

// Ressource pour une API REST sur des categories
@Path("/categories")
public class CategoryResource {

    // Service s'occupant de cet API
    CategoryDAO service = new CategoryDAO();

     // CREATE Methods
    @POST
    public void createCategory(Category _categorie) {
        if(_categorie != null) {
            service.create(_categorie);
        } else {
            System.out.println("EMPTY PARAM");
        }
    }

    // READ Method
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Category> getAllCategory() {
        return service.read();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Category getCategoryById(@PathParam("id") Integer _id) {
        return service.readByID(_id);
    }

    // UPDATE Methods
    @PUT
    @Path("/{id}")
    public void updateCategory(@PathParam("id") Integer _id, Category _categorie) {
        if(_categorie != null) {
            service.update(_id, _categorie);
        } else {
            System.out.println("EMPTY PARAM");
        }
    }

    // DELETE Methods
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/{id}")
    public void deleteArticle(@PathParam("id") Integer _id) {
        if(_id != null) {
            Category _category = service.readByID(_id);
            if(_category != null) {
                service.delete(_category);
            } else {
                System.out.println("ITEM NOT EXISTING : "+_id);
            }
        } else {
            System.out.println("EMPTY PARAM");
        }
    }    
}
