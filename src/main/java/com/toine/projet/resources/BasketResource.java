package com.toine.projet.resources;

import java.util.List;

import com.toine.projet.dao.BasketDAO;
import com.toine.projet.model.BasketItem;

import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

// Ressource pour une API REST sur des articles
@Path("/Paniers")
public class BasketResource {

    //Service s'occupant de cet API 
    BasketDAO service = new BasketDAO();

    // GET Method 
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<BasketItem> getAllBasket(){
        return service.getAllBasket();
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{_articleName}")
    public String getarticleName() {
        return "Voici le panier 1";
    }

    // CREATE Methods
    @POST
    public void createBasket() {
        System.out.println("CREATE NEW BASKET");
    }

    // DELETE Methods
    @DELETE
    @Path("/{_articleName}")
    public void deletePanier(@PathParam("_articleName") String _articleName) {
        System.out.println("DELETE: "+_articleName);
    }

    // UPDATE Methods
    @PUT
    @Path("/{_articleName}")
    public void updatePanier(@PathParam("_articleName") String _articleName) {
        System.out.println("UPDATE: "+_articleName);
    }

}