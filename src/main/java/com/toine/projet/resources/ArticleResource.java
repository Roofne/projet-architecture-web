package com.toine.projet.resources;

import java.util.List;

import com.toine.projet.dao.ArticleDAO;
import com.toine.projet.model.Article;

import jakarta.ws.rs.PUT;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.DELETE;

import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

// Ressource pour une API REST sur des articles
@Path("/articles")
public class ArticleResource {

    // Service s'occupant de cet API
    ArticleDAO service = new ArticleDAO();

    // CREATE Methods
    @POST
    public void createArticle(Article new_article) {
        if(new_article != null) {
            service.create(new_article);
        } else {
            System.out.println("EMPTY PARAM");
        }
    }

    
    // READ Method
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Article> getAllArticles() {
        return service.readAll();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Article getArticlesById(@PathParam("id") Integer _id) {
        return service.read(_id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/category/{id}")
    public List<Article> getArticlesByCategory(@PathParam("id") Integer _id) {
        return service.readByCategory(_id);
    }

    // UPDATE Methods
    @PUT
    @Path("/{id}")
    public void updateArticle(@PathParam("id") Integer _id, Article new_article) {
        if(new_article != null) {
            service.update(_id,new_article);
        } else {
            System.out.println("EMPTY PARAM");
        }
    }

    // DELETE Methods
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/{id}")
    public void deleteArticle(@PathParam("id") Integer _id) {
        if(_id != null) {
            Article old_article = service.read(_id);
            if(old_article != null) {
                service.delete(old_article);
            } else {
                System.out.println("ITEM NOT EXISTING : "+_id);
            }
        } else {
            System.out.println("EMPTY PARAM");
        }
    }
}
