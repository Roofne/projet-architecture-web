package com.toine.projet.resources;

import java.util.List;

import com.toine.projet.dao.UserDAO;
import com.toine.projet.model.User;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

// Ressource pour une API REST sur des categories
@Path("/users")
public class UserResource {

    // Service s'occupant de cet API
    UserDAO service = new UserDAO();
    
    // CREATE Methods
    @POST
    public void createUser(User _user) {
        if(_user != null) {
            service.create(_user);
        } else {
            System.out.println("EMPTY PARAM");
        }
    }

    // READ Method
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> getAllUser() {
        return service.read();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public User getUserById(@PathParam("id") Integer _id) {
        return service.readUserByID(_id);
    }

    // UPDATE Methods
    @PUT
    @Path("/{id}")
    public void updateUser(@PathParam("id") Integer _id, User _user) {
        service.update(_id, _user);
    }

    // DELETE Methods
    @DELETE
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/{id}")
    public void deleteArticle(@PathParam("id") Integer _id) {
        if(_id != null) {
            User old_article = service.readUserByID(_id);
            if(old_article != null) {
                service.delete(old_article);
            } else {
                System.out.println("ITEM NOT EXISTING : "+_id);
            }
        } else {
            System.out.println("EMPTY PARAM");
        }
    }
}
