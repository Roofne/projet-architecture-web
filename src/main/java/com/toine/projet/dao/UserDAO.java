package com.toine.projet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.toine.projet.controller.BDDController;
import com.toine.projet.model.User;

public class UserDAO {
    // Attributes
    public List<User> utilisateurs;
    public BDDController service;
    public String db_table = "User";

    // Constructeur
    public UserDAO() {
        this.utilisateurs = new ArrayList<User>();
        this.service = new BDDController();
    }

    // Methodes
    public User parseDataSQL(ResultSet rs) throws SQLException {
        Integer _id      = rs.getInt("id_u");    
        String  _nom     = rs.getString("firstname");    
        String  _prenom  = rs.getString("surname");
        String  _pseudo  = rs.getString("username"); 
        String  _mail    = "";   
        String  _phone   = "";
        String  _mdp     = rs.getString("pwd");
        Boolean _isAdmin = rs.getBoolean("isAdmin");
        return new User(_id, _nom, _prenom, _pseudo, _mail, _isAdmin);              
    }

    // Methodes CRUD

    // CREATE
    public void create(User _user) {
        if(_user != null) {
            String request = "INSERT INTO User VALUES(NULL,";
            request = request+"'"+_user.getNom()+"',";
            request = request+"'"+_user.getPrenom()+"',";
            request = request+"'"+_user.getPseudo()+"',";
            request = request+"'"+_user.getMdp()+"',";
            request = request+_user.getIsAdmin();
            request += ")";
            System.out.println(request);
            service.create(request);
        } else {
            System.out.println("EMPTY PARAM");
        }
    }

    // READ
    public List<User> read() {
        try {
            ResultSet rs = service.select("SELECT * FROM User");
            if(rs != null) {
                while (rs.next()) {
                    User utilisateur = parseDataSQL(rs);
                    this.utilisateurs.add(utilisateur);    
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this.utilisateurs;
    }

    public User readUserByID(Integer _id) {
        User temp = new User();
        try {
            ResultSet rs = service.select("SELECT * FROM User WHERE id_u="+_id);
            if(rs != null) {
                while (rs.next()) {
                    temp = parseDataSQL(rs); 
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return temp;
    }

    // UPDATE 
    public void update(Integer _id, User _user) {
        String request = "UPDATE User SET ";
        request = request+"firstname='"+_user.getNom()+"' ,";
        request = request+"surname='"+_user.getPrenom()+"' ,";
        request = request+"username='"+_user.getPrenom()+"' ,";
        request = request+"pwd='"+_user.getMdp()+"' ,";
        request = request+"isAdmin="+_user.getIsAdmin();
        request = request+" WHERE id_u="+_id;
        service.update(request);
    }

    // DELETE
    public void delete(User _user) {
        service.delete("DELETE FROM User WHERE id_u="+_user.getId());    
    }
}
