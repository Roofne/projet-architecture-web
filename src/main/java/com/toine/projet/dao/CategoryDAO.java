package com.toine.projet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.toine.projet.controller.BDDController;
import com.toine.projet.model.Category;

public class CategoryDAO {
    // Attributes
    public List<Category> categories;
    public BDDController service;
    public String db_table = "Category";

    // Constructeur
    public CategoryDAO() {
        this.categories = new ArrayList<Category>();
        this.service = new BDDController();
    }

    // METHODES CRUD

    // CREATE
    public void create(Category _categorie) {
        String request = "INSERT INTO Category VALUES(NULL,";
        request = request+"'"+_categorie.getName()+"'";
        request += ")";
        service.create(request);
    }

    // READ
    public List<Category> read() {
        try {
            ResultSet rs = service.select("SELECT * FROM Category");
            if(rs != null) {
                while (rs.next()) {
                    Category categorie = parseDataSQL(rs);
                    this.categories.add(categorie);    
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this.categories;
    }

    public Category readByID(Integer _id) {
        Category temp = new Category();
        try {
            ResultSet rs = service.select("SELECT * FROM Category WHERE id_c="+_id);
            if(rs != null) {
                while (rs.next()) {
                    Category article = parseDataSQL(rs);
                    this.categories.add(article);   
                    temp = article;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return temp;
    }

    // UPDATE 
    public void update(Integer _id, Category _categorie) {
        String request = "UPDATE Category SET ";
        request = request+"name='"+_categorie.getName()+"'";
        request = request+" WHERE id_c="+_id;
        service.update(request);
    }

    // DELETE
    public void delete(Category _categorie) {
        service.delete("DELETE FROM Category WHERE id_c="+_categorie.getId());    
    }

    // Methodes
    public Category parseDataSQL(ResultSet rs) throws SQLException {
        Integer id  = rs.getInt("id_c");    
        String  nom = rs.getString("name");      
        return new Category(id,nom);                      
    }
}
