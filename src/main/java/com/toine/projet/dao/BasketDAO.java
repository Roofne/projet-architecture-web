package com.toine.projet.dao;

import java.util.ArrayList;
import java.util.List;

import com.toine.projet.model.BasketItem;

public class BasketDAO {
    public List<BasketItem> getAllBasket(){
        BasketItem b1 = new BasketItem( "Banane",  5 ,  2.0);
        List<BasketItem> l = new ArrayList<BasketItem>();
        l.add(b1);
        return l;
    }
}
