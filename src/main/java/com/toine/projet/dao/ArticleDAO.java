package com.toine.projet.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.toine.projet.controller.BDDController;
import com.toine.projet.model.Article;

public class ArticleDAO {
    // Attributes
    public List<Article> articles;
    public BDDController service;

    // Constructeur
    public ArticleDAO() {
        this.articles = new ArrayList<Article>();
        this.service  = new BDDController();
    }

    // Methodes
    public Article parseDataSQL(ResultSet rs) throws SQLException {
        Integer id       = rs.getInt("id_a");    
        String  marque   = rs.getString("marque");      
        String  category = "Boisson";     
        Double  prix     = rs.getDouble("prix");                        
        String  photo    = rs.getString("photo");
        return new Article(id,marque,prix,category);                      
    }

    // METHODES CRUD

    // CREATE
    public void create(Article new_article) {
        String request = "INSERT INTO Article VALUES(NULL,";
        request = request+"'"+new_article.getMarque()+"',";
        request = request+new_article.getPrice()+",";
        request = request+new_article.getCategory()+",";
        request = request+"'temp.png'";
        request += ")";
        System.out.println(request);
        service.create(request);
    }

    // READ
    public List<Article> readAll() {
        try {
            ResultSet rs = service.select("SELECT * FROM Article");
            if(rs != null) {
                while (rs.next()) {
                    Article article = parseDataSQL(rs);
                    this.articles.add(article);    
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this.articles;
    }

    public Article read(Integer _id) {
        Article temp = new Article();
        try {
            ResultSet rs = service.select("SELECT * FROM Article WHERE id_a="+_id);
            if(rs != null) {
                while (rs.next()) {
                    Article article = parseDataSQL(rs);
                    this.articles.add(article);   
                    temp = article;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return temp;
    }

    public List<Article> readByCategory(Integer _id) {
        List<Article> temp = new ArrayList<Article>();
        try {
            ResultSet rs = service.select("SELECT * FROM Article a WHERE a.category="+_id);
            if(rs != null) {
                while (rs.next()) {
                    Article article = parseDataSQL(rs);
                    this.articles.add(article);   
                    temp.add(article);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return temp;
    }

    // UDPATE
    public void update(Integer _id, Article new_article) {
        String request = "UPDATE Article SET ";
        request = request+"marque='"+new_article.getMarque()+"' ,";
        request = request+"prix="+new_article.getPrice();
        request = request+" WHERE id_a="+_id;
        service.update(request);
    }

    // DELETE
    public void delete(Article old_article) {
        service.delete("DELETE FROM Article WHERE id_a="+old_article.getId());    
    }
}
