package com.toine.projet.dao;

public class DAO {
    // Attributs
    private String table_name;

    // Constructeurs
    public DAO(String _table) {
        this.table_name = _table;
    }

    // Methodes
    public void create() {}
    public void read() {}
    public void update() {}
    public void delete() {}
}


