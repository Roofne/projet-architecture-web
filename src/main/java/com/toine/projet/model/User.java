package com.toine.projet.model;

public class User {
    // Atributs
    private Integer id;
    private String  nom;
    private String  prenom;
    private String  pseudo;
    private String  mail;
    private String  phone;
    private String  mdp;
    private Boolean isAdmin;

    // Constructeurs
    public User() {
        this.id     = 0;
        this.nom    = "";
        this.prenom = "";
        this.pseudo = "";
        this.mail   = "";
        this.phone  = "";
        this.mdp    = "";
        this.isAdmin = false;
    }

    public User(Integer _id, String _nom, String _prenom, String _pseudo, String _mdp, Boolean _isAdmin) {
        this.id     = _id;
        this.nom    = _nom;
        this.prenom = _prenom;
        this.pseudo = _pseudo;
        this.mail   = null;
        this.phone  = null;
        this.mdp    = _mdp;
        this.isAdmin = false;
    }    

    // Getters et setters
    public Integer getId() { return this.id; }
    public String  getNom() { return this.nom; }
    public String  getPrenom() { return this.prenom; }
    public String  getPseudo() { return this.pseudo; }
    public String  getMail() { return this.mail; }
    public String  getPhone() { return this.phone; }
    public String  getMdp() { return this.mdp; }
    public Boolean getIsAdmin() { return this.isAdmin; }

    public void setId(Integer _id) { this.id=_id; }
    public void setNom(String _nom) { this.nom=_nom; }
    public void setPrenom(String _prenom) { this.prenom=_prenom; }
    public void setPseudo(String _pseudo) { this.pseudo=_pseudo; }
    public void setMail(String _mail) { this.mail=_mail; }
    public void setPhone(String _phone) { this.phone=_phone; }
    public void setMdp(String _mdp) { this.mdp=_mdp; }
    public void setIsAdmin(Boolean _isAdmin) { this.isAdmin=_isAdmin; }
}
