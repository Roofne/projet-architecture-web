package com.toine.projet.model;

public class Article {
    // Atributs
    private Integer id;
    private String marque;
    private Double price;
    private String category;

    // Constructeur
    public Article() {
        this.id = 0;
        this.marque = "Unknown Marque";
        this.price = 0.0;
        this.category = "Unknown Categorie";
    }

    public Article(int _id, String _marque, Double _price, String _category) {
        this.id = _id;
        this.marque = _marque;
        this.price = _price;
        this.category = _category;
    }

    // Getters et setters
    public Integer getId() { return this.id; }
    public String getMarque() { return this.marque; }
    public Double getPrice() { return this.price; }
    public String getCategory() { return this.category; }

    public void setId(Integer _id) { this.id = _id; }
    public void setMarque(String _id) { this.marque = _id; }
    public void setPrice(Double _id) { this.price = _id; }
    public void setCategory(String _id) { this.category = _id; }

    // Methods
    public void update(Article _article) {
        this.id       = _article.getId();
        this.marque   = _article.getMarque();
        this.category = _article.getCategory();
        this.price    = _article.getPrice();
    }

    public String toString() {
        return this.id+" "+this.marque+" "+this.category+" "+this.price;
    }
}
