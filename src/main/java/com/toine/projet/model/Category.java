package com.toine.projet.model;

public class Category {
    // Atributs
    private Integer id;
    private String  name;

    // Constructeurs
    public Category() {
        this.id   = 0;
        this.name = "";
    }

    public Category(String _name) {
        this.id   = 0;
        this.name = _name;
    }  
    
    public Category(Integer _id, String _name) {
        this.id   = _id;
        this.name = _name;
    }  

    // Getters et setters
    public Integer getId() { return this.id; }
    public String getName() { return this.name; }

    public void setId(Integer _id) { this.id=_id; }
    public void setName(String _name) { this.name=_name; }

}
