package com.toine.projet.model;

import java.util.ArrayList;
import java.util.List;

public class Basket {
   private List<BasketItem> basketItems;
   private double totalPrice;

   public Basket() {
      basketItems = new ArrayList<>();
      totalPrice = 0.0;
   }

   public void addArticle(String articleName, int quantity, double price) {
      BasketItem basketItem = new BasketItem(articleName, quantity, price);
      basketItems.add(basketItem);
      totalPrice += basketItem.getTotalPrice();
   }

   public void modifyArticle(int index, String articleName, int quantity, double price) {
      BasketItem basketItem = basketItems.get(index);
      totalPrice -= basketItem.getTotalPrice();
      basketItem.setArticleName(articleName);
      basketItem.setQuantity(quantity);
      basketItem.setPrice(price);
      totalPrice += basketItem.getTotalPrice();
   }

   public void removeArticle(int index) {
      BasketItem basketItem = basketItems.remove(index);
      totalPrice -= basketItem.getTotalPrice();
   }

   public double getTotalPrice() {
      return totalPrice;
   }

   private static class BasketItem {
      private String articleName;
      private int quantity;
      private double price;

      public BasketItem(String articleName, int quantity, double price) {
         this.articleName = articleName;
         this.quantity = quantity;
         this.price = price;
      }

      public double getTotalPrice() {
         return quantity * price;
      }

      public void setArticleName(String articleName) {
         this.articleName = articleName;
      }

      public void setQuantity(int quantity) {
         this.quantity = quantity;
      }

      public void setPrice(double price) {
         this.price = price;
      }
   }
}