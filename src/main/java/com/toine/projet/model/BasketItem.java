package com.toine.projet.model;

public class BasketItem {
        private String articleName;
        private int quantity;
        private double price;

        public BasketItem(){
            this.articleName = "Unknown Article";
            this.quantity = 0 ;
            this.price = 0.0;
        }
  
        public BasketItem(String _articleName, int _quantity, double _price) {
           this.articleName = _articleName;
           this.quantity = _quantity;
           this.price = _price;
        }
  
        public double getTotalPrice() {
           return quantity * price;
        }

        public String getarticleName(){
            return this.articleName;
        }

        public Integer getquantity(){
            return this.quantity;
        }

        public Double getprice(){
            return this.price;
        }

        public void setArticleName(String _articleName) {
           this.articleName = _articleName;
        }
  
        public void setQuantity(int _quantity) {
           this.quantity = _quantity;
        }
  
        public void setPrice(double _price) {
           this.price = _price;
        }
     }

